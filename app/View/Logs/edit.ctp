<div class="logs form">
<?php echo $this->Form->create('Log'); ?>
	<fieldset>
		<legend><?php echo __('Edit Log'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('product_code');
		echo $this->Form->input('customer_number');
		echo $this->Form->input('trx_type');
		echo $this->Form->input('request');
		echo $this->Form->input('response');
		echo $this->Form->input('request_date');
		echo $this->Form->input('response_date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Log.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Log.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Logs'), array('action' => 'index')); ?></li>
	</ul>
</div>
