<div class="logs view">
<h2><?php echo __('Log'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($log['Log']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product Code'); ?></dt>
		<dd>
			<?php echo h($log['Log']['product_code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Customer Number'); ?></dt>
		<dd>
			<?php echo h($log['Log']['customer_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Trx Type'); ?></dt>
		<dd>
			<?php echo h($log['Log']['trx_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Request'); ?></dt>
		<dd>
			<?php echo h($log['Log']['request']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response'); ?></dt>
		<dd>
			<?php echo h($log['Log']['response']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Request Date'); ?></dt>
		<dd>
			<?php echo h($log['Log']['request_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Date'); ?></dt>
		<dd>
			<?php echo h($log['Log']['response_date']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Log'), array('action' => 'edit', $log['Log']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Log'), array('action' => 'delete', $log['Log']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $log['Log']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Logs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Log'), array('action' => 'add')); ?> </li>
	</ul>
</div>
