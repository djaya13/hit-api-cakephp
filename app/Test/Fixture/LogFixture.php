<?php
/**
 * Log Fixture
 */
class LogFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'product_code' => array('type' => 'integer', 'null' => false, 'default' => null),
		'customer_number' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 16),
		'trx_type' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 4),
		'request' => array('type' => 'text', 'null' => false, 'default' => null, 'length' => 1073741824),
		'response' => array('type' => 'text', 'null' => true, 'default' => null, 'length' => 1073741824),
		'request_date' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'response_date' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'product_code' => 1,
			'customer_number' => 'Lorem ipsum do',
			'trx_type' => 'Lo',
			'request' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'response' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'request_date' => '2019-04-30 11:20:55',
			'response_date' => '2019-04-30 11:20:55'
		),
	);

}
