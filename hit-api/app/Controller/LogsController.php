<?php
App::uses('AppController', 'Controller');

date_default_timezone_set("Asia/Jakarta");
/**
 * Logs Controller
 *
 * @property Log $Log
 * @property PaginatorComponent $Paginator
 */
class LogsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');


	public function hitApi($params = array()) {
		// set paramete header
		$this->pel_user = "plg12110004";
		$this->pel_pass = "aldwin.3";
		$this->pel_key = "1234";
		$this->pel_url = "http://202.152.60.61:8118/Transactions/trx.json";


		$trx_date = $params['trx_date'];
		$trx_id = $params['trx_id'];
		$trx_type = $params['trx_type'];
		$cust_msisdn = $params['cust_msisdn'];
		$cust_account_no = $params['cust_account_no'];
		$product_id = $params['product_id'];
		$product_nomination = $params['product_nomination'];
		$periode_payment = $params['periode_payment'];
		$unsold = $params['unsold'];

		$signature = md5($this->pel_user . $this->pel_pass . $product_id . $trx_date . $this->pel_key);

		$post_data = array(
			'trx_date' => $trx_date,
			'trx_type' => $trx_type,
			'trx_id' => $trx_id,
			'cust_msisdn' => $cust_msisdn,
			'cust_account_no' => $cust_account_no,
			'product_id' => $product_id,
			'product_nomination' => $product_nomination,
			'periode_payment' => $periode_payment,
			// 'unsold' => '1'
		);

		$post_data = http_build_query($post_data, '', '&');
		$curl = curl_init($this->pel_url);

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_VERBOSE, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_URL, $this->pel_url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		    'Authorization: PELANGIREST username='.$this->pel_user.'&password='.$this->pel_pass.'&signature='.$signature
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);

		$response = curl_exec($curl);
		$info = curl_getinfo($curl);

		curl_close($curl);

		return $response;
	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Log->recursive = 0;
		$this->set('logs', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Log->exists($id)) {
			throw new NotFoundException(__('Invalid log'));
		}
		$options = array('conditions' => array('Log.' . $this->Log->primaryKey => $id));
		$this->set('log', $this->Log->find('first', $options));
	}

	public function generateTrxId($length) {
		return substr(str_shuffle("0123456789"), 0, $length);
	}


/**
 * add method
 *
 * @return void
 */
	// public function add() {
	// 	if ($this->request->is('post')) {
	// 		$this->Log->create();
	// 		if ($this->Log->save($this->request->data)) {
	// 			$this->Flash->success(__('The log has been saved.'));
	// 			return $this->redirect(array('action' => 'index'));
	// 		} else {
	// 			$this->Flash->error(__('The log could not be saved. Please, try again.'));
	// 		}
	// 	}
	// }


	public function add() {
		if ($this->request->is('post')) {
			// set parameter
			$params['trx_date'] = date("YmdHis");
			$params['trx_id'] = $this->generateTrxId(10);
			$params['trx_type'] = '2100'; // 2100 = Inquiry, 2200 = Payment
			$params['cust_msisdn'] = '01428800711';
			$params['cust_account_no'] = $this->request->data['Log']['customer_number'];
			$params['product_id'] = '80'; // 80 = PLN Prepaid
			$params['product_nomination'] = ''; 
			// $params['product_nomination'] = '20000'; // use this line to makes payment request
			$params['periode_payment'] = '';
			$params['unsold'] = '';



			// send to get data
			$request_date = date('Y-m-d H:i:s');
			$output = $this->hitApi($params);
			$input = json_encode($params, true);


			$parse = json_decode($output, true);
			$data = $parse['data']['trx'];

			$response_date = date('Y-m-d H:i:s');

			// set save api to local
			$save = $this->Log->save(
						array(
						'product_code' 		=> $params['product_id'],
						'customer_number' 	=> $this->request->data['Log']['customer_number'],
						'trx_type' 			=> $params['trx_type'],
						'request' 			=> $input,
						'response' 			=> $output,
						'request_date' 		=> $request_date,
						'response_date' 	=> $response_date
						)
					);

			if ($save) {
				$this->Flash->success(__('The log has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The log could not be saved. Please, try again.'));
			}
		}
	}





/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Log->exists($id)) {
			throw new NotFoundException(__('Invalid log'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Log->save($this->request->data)) {
				$this->Flash->success(__('The log has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The log could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Log.' . $this->Log->primaryKey => $id));
			$this->request->data = $this->Log->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->Log->exists($id)) {
			throw new NotFoundException(__('Invalid log'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Log->delete($id)) {
			$this->Flash->success(__('The log has been deleted.'));
		} else {
			$this->Flash->error(__('The log could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
